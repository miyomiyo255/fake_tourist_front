package com.example.webdictionary

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_sub.*

import android.os.AsyncTask
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.content.Intent
import kotlinx.android.synthetic.main.activity_main.*
import com.google.gson.JsonArray
import okhttp3.OkHttpClient
import okhttp3.Request
import com.google.gson.JsonObject
import com.google.gson.JsonParser

val url_full :String = "http://localhost:8080/add/2/3"

/*この関数は可読性が△な気がします... queryをonClickListenerで宣言してその時url_fullが定められそのあとMyAsyncTaskがそこにアクセスします*/
fun getHtml(): String {
    val client = OkHttpClient()
    val req = Request.Builder().url(url_full).get().build()
    val resp = client.newCall(req).execute()
    return resp.body()!!.string()
}

class SubActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub)

        val editable: EditText = findViewById<EditText>(R.id.edit_text)
        val btn: Button = findViewById(R.id.button)

        btn.setOnClickListener{
            val tview = findViewById<TextView>(R.id.textView)
            val conts = "hello"
            tview.setText(conts)

            /*
            returnButton.setOnClickListener{
                finish()
            */
            }

        }
}

/*
    /*このAsyncTaskについて怒られてるのであとで直します*/
    class MyAsyncTask: AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg p0: Void?): String? {
            return ("")
        }


        override fun onPostExecute(result: String?) {
            /*
            super.onPostExecute(result)
            val tview = findViewById<TextVihello>(R.id.textView)
            val myJson :JsonObject = JsonParser().parse(result).asJsonObject
            val my_tuc :JsonArray = myJson.getAsJsonArray("tuc").asJsonArray

            /*このへんの型変換もなんとなくごちゃごちゃしてますね...うまいやり方を以後考えます*/
            var conts = "ヒット結果:\n"
            for (i in 0..(my_tuc.size()) - 1)
            {
                val my_tuc_i_json :JsonObject = JsonParser().parse(my_tuc[i].toString()).asJsonObject
                val my_phrase_i :JsonObject? = my_tuc_i_json.getAsJsonObject("phrase")
                if (my_phrase_i != null)
                    conts = conts + "/" + my_phrase_i.getAsJsonPrimitive("text").asString
            }*/
            val tview = findViewById<TextView>(R.id.textView)
            val conts = "hi"
            tview.setText(conts)
        }
    }

}*/