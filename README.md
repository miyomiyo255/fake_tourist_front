<img src="https://user-images.githubusercontent.com/43234619/52190463-a7f19e00-2882-11e9-9ad3-fed902b45625.png" width="320px">

# Web辞書
glosbeさまのAPIを使った簡単なWeb辞書

# Dependency
使用言語 Kotlin 1.3　　
okhttpを使用　　

# Usage
検索バーに英単語を入れてボタンを押すと英単語の意味が表示されます　　
スペルチェックは今後実装します　　

# 今後の展望
英単語の画像検索結果も一緒に表示して、視覚的に英単語の意味を覚えられるようにします

# Authors
三好壮哉

